package org.andre.tcc.android;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.Calendar;

import org.andre.tcc.jpa.entity.Aviso;
import org.andre.tcc.jpa.entity.Meteorologia;
import org.andre.tcc.jpa.enumeration.EnumAviso;
import org.andre.tcc.jpa.enumeration.EnumDirecaoVento;
import org.andre.tcc.jpa.enumeration.EnumTempo;
import org.andre.tcc.jpa.xml.XMLUtil;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class Home extends Activity {
	
	private boolean debug = false;
	
	private static long tempoRefresh = 30 * 60 * 60;
	private Location location;
	
	private static final String SOAP_ACTION = "http://andresilvaalves.no-ip.org:8082/WSTCCAndreService/WSTCCAndre?wsdl";
	private static final String METHOD_NAME = "solicitaDadosLocalizacao";
	private static final String NAMESPACE = "http://webservice.core.tcc.andre.org/";
	
	private static final String URL = "http://andresilvaalves.no-ip.org:8082/WSTCCAndreService/WSTCCAndre?wsdl";
//	private static final String URL = "http://192.168.1.102:8082/WSTCCAndreService/WSTCCAndre?wsdl";
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menus, menu);
        return true;
    }
  
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case R.id.menuAtualizar:
            AtualizaInformacoes();
            return true;
        case R.id.menuSair:
        	System.exit(0);
        	return true;
        default:
            return super.onOptionsItemSelected(item);
        }
    }
    
    @Override
    protected void onStart() {
    	super.onStart();
    	
    	atualizaImagemNaTela(R.drawable.indefinida);

        atualizaMensagemNaTela("Buscando informa��es geogr�ficas...");
        
    	//busca a localiza��o do GPS
    	buscaLocalizacao();
	
    }
    
    
    private void atualizaImagemNaTela(int imagem) {
    	ImageView imagemTempo = (ImageView) findViewById(R.id.imageTempo);
        imagemTempo.setImageResource(imagem);
	}

	private void atualizaMensagemNaTela(String mensagem) {
        TextView tvMensagem = (TextView) findViewById(R.id.textMensagem);
        tvMensagem.setText(mensagem);
	}
	
	private void atualizaLocalizacao(){
		TextView latitude = (TextView) findViewById(R.id.txtLatitude);
		TextView longitude = (TextView) findViewById(R.id.txtLongitude);
		if(this.location != null){
            latitude.setText(""+this.location.getLatitude());
            longitude.setText(""+this.location.getLongitude());	
		}else{
			latitude.setText("0.00000");
            longitude.setText("0.00000");	
		}
	}

	/**
     * Atualiza informa�oes
     */
    private void AtualizaInformacoes() {
    	Aviso aviso = null;
//    	buscaLocalizacao();
    	if(this.location != null){
            aviso = leiaInformacoesWS(chamaWS());
    	}else{
    		atualizaMensagemNaTela("N�o foi poss�vel buscar atualiza��es, localiza��o n�o definida!");
//    		aviso = retornaAvisoTeste();
    		  aviso = leiaInformacoesWS(chamaWS());
    	}
    	atualizaTela(aviso);
    	
	}


    /**
     * Le XML de resposta e constroi um Aviso
     * @param xml
     * @return
     */
	private Aviso leiaInformacoesWS(String xml) {
		Aviso aviso = null;
		try {
			if(xml != null && xml.length() > 0){
				xml = xml.replace("</S:Envelope>","")
				.replace("</S:Body>","")
				.replace("</ns2:solicitaDadosLocalizacaoResponse>","")
				.replace("<ns2:solicitaDadosLocalizacaoResponse xmlns:ns2=\"http://webservice.core.tcc.andre.org/\">","")
				.replace("<S:Envelope xmlns:S=\"http://schemas.xmlsoap.org/soap/envelope/\">", "")
				.replace("<S:Body>","");
//				aviso = FuncoesXML.DeserializeAviso(xml);
				aviso = XMLUtil.retorneAviso(xml);
			}			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return aviso;
		
	}

	/**
	 * Metodo que recebe um objeto aviso e atualiza a tela
	 * @param aviso
	 */
	private void atualizaTela(Aviso aviso) {
		if(aviso != null){
			
			
			StringBuffer mensagem = new StringBuffer();
			mensagem.append(aviso.getMensagem());
//			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
//			mensagem.append("\n Data = "+sdf.format(aviso.getData()));
			mensagem.append("\n Tempo = "+aviso.getMeteorologia().getTempo().getDescricaoTempo());
			mensagem.append("\n Id = "+aviso.getId());
			mensagem.append("\n Distancia Latitude = "+aviso.getDistanciaLatitude());
			mensagem.append("\n Distancia Longitude = "+aviso.getDistanciaLongitude());

			mensagem.append("\n Latitude = "+aviso.getLatitude());
			mensagem.append("\n Longitude = "+aviso.getLongitude());
			mensagem.append("\n Tipo Aviso = "+aviso.getTipoAviso().getDescricaoTipoAviso());
			mensagem.append("\n Velocidade Vento = "+aviso.getMeteorologia().getVelocidadeVento());
			mensagem.append("\n Dire��o Vento = "+aviso.getMeteorologia().getDirecaoVento().getDescricaoDirecaoVento());
			atualizaMensagemNaTela(mensagem.toString());
			
			TextView txtAviso = (TextView) findViewById(R.id.txtAviso);
			txtAviso.setText(aviso.getAviso().getDescricaoAviso());
			EnumAviso enumAviso = aviso.getAviso();
			switch (enumAviso) {
			case AVISO_ALTO:
			case AVISO_MUITO_ALTO:
				txtAviso.setTextColor(Color.RED);
				break;
			case AVISO_MEDIO:	
			case SEM_AVISO:
			case AVISO_BAIXO:
			default:
				txtAviso.setTextColor(Color.WHITE);;
			}
			
			TextView txtMaxima = (TextView) findViewById(R.id.txtMaxima);
			txtMaxima.setText(aviso.getMeteorologia().getTemperaturaMaxima()+"�C");
			
			TextView txtMinima = (TextView) findViewById(R.id.txtMinima);
			txtMinima.setText(aviso.getMeteorologia().getTemperaturaMinima()+"�C");
			
			
			EnumTempo codigoTempo = aviso.getMeteorologia().getTempo();
			switch (codigoTempo) {
			case CHUVA:
				atualizaImagemNaTela(R.drawable.rain);
				break;
			case TROVOADA:
				atualizaImagemNaTela(R.drawable.thunderstorms);
				break;
			case NUBLADO:
				atualizaImagemNaTela(R.drawable.mostly_cloudy);
				break;
			case PARCIALMENTE_NUBLADO:
				atualizaImagemNaTela(R.drawable.cloudy);
				break;
			case SOL:
				atualizaImagemNaTela(R.drawable.sunny);
				break;
			default:
				atualizaImagemNaTela(R.drawable.icon);
				break;
			}
		}else{
			atualizaMensagemNaTela("N�o conseguiu receber aviso do servidor.");
		}
		
	}



	/**
	 * M�todo que faz a chamada do WS(SOAP)
	 * @return
	 */
	public String chamaWS(){
		String resposta = "";
		if(this.location != null || debug){
			try {
				URL url;
				HttpURLConnection connection = null;
				// Create connection
				String targetURL = URL;
				double latitude;
				double longitude;
				
				if(debug){
					latitude = -27.199211;
					longitude = -48.498695;
				}else{
					latitude = this.location.getLatitude();
					longitude = this.location.getLongitude();
				}
				
				String urlParameters = montaXMLSolicitaDadosLocalizacao(
						latitude, longitude);
				
				url = new URL(targetURL);
				connection = (HttpURLConnection) url.openConnection();
				connection.setRequestMethod("POST");
				connection.setRequestProperty("Content-Type", "text/xml");
				
				// connection.setRequestProperty("Content-Length",
				// "" + Integer.toString(urlParameters.getBytes().length));
				connection.setRequestProperty("Content-Language", "en-US");
				connection.setUseCaches(false);
				connection.setDoInput(true);
				connection.setDoOutput(true);
				// Send request
				DataOutputStream wr = new DataOutputStream(
						connection.getOutputStream());
				wr.writeBytes(urlParameters);
				wr.flush();
				wr.close();	
				atualizaMensagemNaTela("Aguardando resposta do servidor...");
				// Get Response
				InputStream is = connection.getInputStream();
				resposta = convertStreamToString(is);//resposta do WS
				
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (ProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return resposta;
	}
	
	
	private void makeUseOfNewLocation(Location location) {
		//Seta a localiza��o da latitude e longitude
		this.location = location;
    	
    	if(this.location != null){
    		atualizaLocalizacao();
    		atualizaMensagemNaTela("Localiza��o encontrada!");
    	}else{
    		atualizaMensagemNaTela("Localiza��o n�o encontrada. Verifique se o GPS est� habilitado!");
    		atualizaLocalizacao();
    	}
	}
	
	/**
	 * Cria um listener que ir� ficar escutando a localiza��o do GPS e atualizando em uma variavel global
	 */
	private void buscaLocalizacao() {
		
		LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
		
		
		Location lastKnownLocation = locationManager.getLastKnownLocation( LocationManager.NETWORK_PROVIDER);
		if(lastKnownLocation != null){
			makeUseOfNewLocation(lastKnownLocation);
		}
		
		LocationListener locationListener = new LocationListener() {

			@Override
			public void onLocationChanged(Location location) {
				makeUseOfNewLocation(location);
				String Text = "Localiza��o: " +
				"Latitud = " + location.getLatitude() +
				"Longitud = " + location.getLongitude();

//				Toast.makeText( getApplicationContext(),
//				Text,
//				Toast.LENGTH_SHORT).show();
			}
		
			@Override
			public void onProviderDisabled(String provider) {
				Toast.makeText( getApplicationContext(),
						"Gps Desligado",
						Toast.LENGTH_SHORT).show();
			}
		
			@Override
			public void onProviderEnabled(String provider) {
				Toast.makeText( getApplicationContext(),
						"Gps Habilitado",
						Toast.LENGTH_SHORT).show();
			}
		
			@Override
			public void onStatusChanged(String provider, int status, Bundle extras) {
			}

		};
		locationManager.removeUpdates(locationListener);
		locationManager.requestLocationUpdates( LocationManager.GPS_PROVIDER, 0, 0, locationListener);
	}

	/**
	 * Metodo respons�vel por montar o XML de requisi��o do WS (SOAP)
	 * @param latitude
	 * @param longitude
	 * @return
	 */
	private String montaXMLSolicitaDadosLocalizacao(double latitude, double longitude) {
		StringBuffer xml = new StringBuffer("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" ");
		xml.append("xmlns:web=\"http://webservice.core.tcc.andre.org/\">");
		xml.append(" <soapenv:Header> ");
		xml.append(" </soapenv:Header>");
		xml.append(" <soapenv:Body> ");
		xml.append("<web:solicitaDadosLocalizacao>");

		xml.append("<latitude>");
		xml.append(latitude);
		xml.append("</latitude>");

		xml.append("<longitude>");
		xml.append(longitude);
		xml.append("</longitude>");

		xml.append("</web:solicitaDadosLocalizacao>");
		xml.append("");
		xml.append(" </soapenv:Body>");
		xml.append("</soapenv:Envelope> ");
		return xml.toString();
	}
	
	
	/**
	 * Ir� converter um InputStream em XML
	 * @param is
	 * @return
	 * @throws IOException
	 */
	public String convertStreamToString(InputStream is) throws IOException {

		if (is != null) {
			Writer writer = new StringWriter();

			char[] buffer = new char[1024];
			try {
				Reader reader = new BufferedReader(new InputStreamReader(is));
				int n;
				while ((n = reader.read(buffer)) != -1) {
					writer.write(buffer, 0, n);
				}
			} finally {
				is.close();
			}
			return writer.toString();
		} else {
			return "";
		}
	}
    
	
	
	//TODO Area de teste
	
	private Aviso retornaAvisoTeste(){
		Aviso aviso = new Aviso();
		aviso.setLatitude(123.45);
		aviso.setAviso(EnumAviso.AVISO_ALTO);
		aviso.setLongitude(789.123);
		aviso.setDescricao("Descri��o do Aviso");
		aviso.setData(Calendar.getInstance());
		aviso.setMensagem("Localiza��o N�o dispon�vel, est� retornando uma entidade aviso para testes...");
		Meteorologia meteorologia = new Meteorologia();
		meteorologia.setVelocidadeVento(50);
		meteorologia.setFenomeno("Sol");
		meteorologia.setTemperaturaMaxima(28);
		meteorologia.setTemperaturaMinima(18);
		meteorologia.setDirecaoVento(EnumDirecaoVento.NORTE);
		meteorologia.setTempo(EnumTempo.CHUVA);
		aviso.setMeteorologia(meteorologia);
		return aviso;
	}
	
	

	
	
}