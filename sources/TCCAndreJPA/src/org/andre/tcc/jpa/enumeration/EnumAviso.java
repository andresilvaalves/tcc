package org.andre.tcc.jpa.enumeration;

public enum EnumAviso {

	
	SEM_AVISO(1,"Sem Aviso"), 
	AVISO_BAIXO(2,"Baixo"),
	AVISO_MEDIO(3,"M�dio"), 
	AVISO_ALTO(4,"Aviso Alto"), 
	AVISO_MUITO_ALTO(5,"Aviso Muito Alto");
	
	
	private int codigoAviso;
	private String descricaoAviso;
	
	private  EnumAviso(int codigoAviso, String descricaoAviso){
		this.codigoAviso = codigoAviso;
		this.descricaoAviso = descricaoAviso;
	}

	public int getCodigoAviso() {
		return this.codigoAviso;
	}

	public String getDescricaoAviso() {
		return descricaoAviso;
	}
	
}
