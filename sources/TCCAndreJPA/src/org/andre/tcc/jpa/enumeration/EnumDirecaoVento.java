package org.andre.tcc.jpa.enumeration;

public enum EnumDirecaoVento {
	
	INDEFINIDO(-1,"Indefinido"),
	NORTE(1,"Norte"), 
	SUL(2,"Sul"), 
	LESTE(3,"Leste"),
	OESTE(4,"Oeste");
	
	private int codigoDirecaoVento;
	private String descricaoDirecaoVento;
	
	private  EnumDirecaoVento(int codigoDirecaoVento, String descricaoDirecaoVento){
		this.codigoDirecaoVento = codigoDirecaoVento;
		this.descricaoDirecaoVento = descricaoDirecaoVento;
	}

	public int getCodigoDirecaoVento() {
		return this.codigoDirecaoVento;
	}

	public String getDescricaoDirecaoVento() {
		return descricaoDirecaoVento;
	}

}
