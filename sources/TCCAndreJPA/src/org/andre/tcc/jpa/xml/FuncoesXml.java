package org.andre.tcc.jpa.xml;


import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.CDATASection;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.Text;


public class FuncoesXml {


  /**
   * Mantido por questoes de compatibilidade com aplica�oes atuais.
   * Assume que buscaSubNivel = true
   * @param tagName
   * @param noPai
   * @return
   */
  public static Element getElementoXml( String tagName, Element noPai ) {
    return getElementoXml(tagName, noPai, true);
  }
  /**
   * getElementoXml:
   * retorna em oElem o elemento com nome = tagName dentro do escopo do noPai
   * @param tagName Nome da TAG que estara sendo buscada
   * @param noPai N� pai que sera utilizado para definir o escopo
   * @param buscaNosFilhos Define se a procura sera feita so no mesmo nivel do noPai ou se entrara nos subniveis
   * @return Elemento encontrado segundo TAG. Caso nao encontre, retorna null
   */
  public static Element getElementoXml( String tagName, Element noPai,
                                        boolean buscaNosFilhos ) {
    Element oElem = null;

    if ( (noPai != null) && (tagName != null) && (!tagName.equals("")) ) {
      oElem = noPai;
      //se o elemento atual n�o for o procurado
      if ( ! getTagElemento(oElem).equalsIgnoreCase(tagName) ) {
        if (buscaNosFilhos) {
          if (oElem.hasChildNodes()) {
            //pega o primeiro no filho que seja da classe Element
            Element oFilho = getFirstElement(oElem);
            try {
              if (oFilho == null)
                return null; //se na lista de filhos nao tem um objeto Element, retorne vazio
              //aplica a busca recursiva para localizar o elemento
              do {
                oElem = getElementoXml(tagName, oFilho, buscaNosFilhos);
                oFilho = getNextElementSibling(oFilho);
              } while ( (oFilho != null) && (oElem == null)); //se nao achar oElem retornara null

            } catch (ClassCastException ex) {
              return null; //nao faz nada...  segue adiante.
            }
          } else //se o elemento nao tiver filhos...
            return null;
        } else //se nao � para buscar o elemento nos filhos, verifica no irmao
          return getElementoXml(tagName, getNextElementSibling(oElem), buscaNosFilhos);
      } else {//se eh o elemento procurado
        return oElem;
      }
    }
    return oElem;
  }

  /***
   * transformInputStreamToDocument:
   * Transforma um InputStream em um documento XML
   * @param xmlFile InputStream que ser� retornada
   * @return Document Documento equivalente ao InputStream
   * @throws Exception
   */
  public static Document transformInputStreamToDocument(InputStream xmlFile) throws Exception{
    Document XmlDoc = null;
    try {
        DocumentBuilderFactory docFabrica = DocumentBuilderFactory.newInstance();
        DocumentBuilder xmlLeitura = docFabrica.newDocumentBuilder();
        XmlDoc = xmlLeitura.parse(xmlFile);
    }catch(Exception e){
      throw new Exception("Erro ao transformar InputStreamToDocument - "+e.getMessage());
   }
   return XmlDoc;
  }


  /***
   * transformDocumentToInputStream
   *
   * Objetivo: transformar um documento Document em um InputStream
   * @param Document Doc
   */
  public static InputStream transformDocumentToInputStream(Document Doc) throws Exception{
    InputStream resposta = null;
    try{
      ByteArrayOutputStream xmlBytes =  new ByteArrayOutputStream();
      byte bXml[];
      TransformerFactory transFabrica = TransformerFactory.newInstance();
      Transformer xmlWriter = transFabrica.newTransformer();
      xmlWriter.setOutputProperty("encoding","UTF-8");
      //as 2 proximas linhas sao para colocar \n no arquivo e ident�-lo
      xmlWriter.setOutputProperty(OutputKeys.INDENT, "yes");
      xmlWriter.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");

      xmlWriter.transform(new DOMSource(Doc), new StreamResult(xmlBytes));
      bXml = xmlBytes.toByteArray();
      resposta = new ByteArrayInputStream(bXml);
    }catch(Exception e){
      if (e instanceof NullPointerException)
        throw new Exception("Erro ao transformar DocumentToInputStream - valor ou atributo de algum elemento � nulo!");
      else
        throw new Exception("Erro ao transformar DocumentToInputStream - "+e.getMessage());
    }
    return resposta;
  }

  /**
   * criaXmlDocument
   * @return Document XML
   * @throws Exception
   */
  public static Document criaXmlDocument() throws Exception{
    Document XmlDoc = null;
    try{
      DocumentBuilderFactory docFabrica = DocumentBuilderFactory.newInstance();
      DocumentBuilder xml = docFabrica.newDocumentBuilder();
      XmlDoc = xml.newDocument();
    }catch(Exception e){
      throw new Exception("Erro ao criar o documento XML - "+e.getMessage());
    }
    return XmlDoc;
  }

  /**
   * getFirstElement
   * @param noPai
   * @return
   */
  public static Element getFirstElement( Node noPai ) {
    Node noFilho = noPai.getFirstChild();
    //percorre a arvore ateh achar um Elemento que seja filho, ignorando os nos texto
    while ( (noFilho != null) && !(noFilho instanceof Element) )
      noFilho = noFilho.getNextSibling();

    if (noFilho != null)
      return (Element)noFilho; //pega o primeiro Elemento filho do elemento atual
    else
      return null; //se na lista de filhos nao tem um objeto Element, retorne vazio
  }

  /**
   * getNextElementSibling
   * @param noIrmao
   * @return
   */
  public static Element getNextElementSibling( Node noIrmao ) {
    noIrmao = noIrmao.getNextSibling();
    //percorre a arvore ateh achar um Elemento que seja filho, ignorando os nos texto
    while ( (noIrmao != null) && !(noIrmao instanceof Element) )
      noIrmao = noIrmao.getNextSibling();

    if (noIrmao != null)
      return (Element)noIrmao;
    else
      return null; //se na lista de irmaos nao tem um objeto Element, retorne vazio
  }

  /**
   *
   * @param noIrmao
   * @return
   */
  public static Element getPreviousElementSibling( Node noIrmao ) {
    noIrmao = noIrmao.getPreviousSibling();
    //percorre a arvore ateh achar um Elemento que seja filho, ignorando os nos texto
    while ( (noIrmao != null) && !(noIrmao instanceof Element) )
      noIrmao = noIrmao.getPreviousSibling();

    if (noIrmao != null)
      return (Element)noIrmao;
    else
      return null; //se na lista de irmaos nao tem um objeto Element, retorne vazio
  }

  /**
   * getElementoXmlEx
   * @param tag
   * @param pai
   * @return
   * @throws Exception
   */
  public static Element getElementoXmlEx(String tag, Element pai) throws Exception {
    Element resposta = FuncoesXml.getElementoXml(tag, pai);
    if ( resposta == null )
      throw new Exception("Elemento nao existe: "+tag);

    return resposta;
  }

  /**
   * setValorElementoXml
   * @param oElem
   * @param valor
   * @param xmlDoc
   * @throws Exception
   */
  public static void setValorElementoXml( Element oElem, String valor, Document xmlDoc ) throws Exception{
    if (valor != null) {
      Text noTexto = xmlDoc.createTextNode(valor);
      oElem.appendChild(noTexto);
    } else
      throw new Exception("Nao pode atribuir valor nulo ao elemento: "+oElem.getTagName());
  }

  /**
   * addCDATAtoElementoXml
   * @param oElem
   * @param valor
   * @param xmlDoc
   * @throws Exception
   */
  public static void addCDATAtoElementoXml( Element oElem, String valor, Document xmlDoc ) throws Exception{
    CDATASection noCDATA = xmlDoc.createCDATASection(valor);
    oElem.appendChild(noCDATA);
  }

  /**
   * getCDATAValue
   * @param __elemento
   * @return
   */
  public static String getCDATAValue(Element __elemento) {
    if (__elemento.hasChildNodes()) {
      //pego o elemento filho do tipo CDATA
      Node CDATASection = getFirstChildCDATA(__elemento);
      //retorna o valor do n�

      return CDATASection.getNodeValue();

    }
    return null;
  }

  /**
   * getFirstChildCDATA
   * @param __noPai
   * @return
   */
  private static Node getFirstChildCDATA(Element __noPai) {
    Node no = __noPai.getFirstChild();
    while (no!=null) {
      if (no.getNodeType()==Node.CDATA_SECTION_NODE)
        return no;
      no=no.getNextSibling();
    }
    return null;
  }

  /**
   * Retorna o valor de um elemento, onde <elemento>valor</elemento>.
   * @param oElem
   * @param geraErro indica se deve gerar Exception caso o valor seja nulo
   * @return
   * @throws Exception
   */
  public static String getValorElementoXml( Element oElem, boolean geraErro ) throws Exception{
    Node oFilho = oElem.getFirstChild();

    while ( (oFilho != null) && !(oFilho instanceof Text) )
      oFilho = oFilho.getNextSibling();

    if (oFilho != null)
      return ((Text)oFilho).getData();
    else {
      if (geraErro)
        throw new Exception ("Elemento n�o possui valor: "+oElem.getTagName());
      return null;
    }
  }


  /**
   * getXMLDocFromFileName()
   * recupera o Documento xml do arquivo de requisicao
   * @return Documento xml
   * @throws Exception
   */
  public static Document getXMLDocFromFileName(String __nomeArquivo) throws Exception {
    //Faz o parse do arquivo de requisicao
    DocumentBuilderFactory docFabrica = DocumentBuilderFactory.newInstance();
    DocumentBuilder xml = docFabrica.newDocumentBuilder();
    Document xmlDoc = xml.parse(__nomeArquivo);

    return xmlDoc;
  }

  /**
   * salvaDocumentEmArquivo()
   * grava o conteudo da resposta no arquivo de resposta
   * @param XmlDoc
   * @param arquivo
   */
  public static void salvaDocumentEmArquivo(Document XmlDoc, File arquivo) throws Exception{
    try{
      //se o arquivo existir, o apaga
      if (arquivo.exists())
        arquivo.delete();
      //salva o Document no Arquivos
      TransformerFactory transFabrica = TransformerFactory.newInstance();
      Transformer xmlWriter = transFabrica.newTransformer();
      xmlWriter.setOutputProperty("encoding","UTF-8");
      //as 2 proximas linhas sao para colocar \n no arquivo e ident�-lo
      xmlWriter.setOutputProperty(OutputKeys.INDENT, "yes");
      xmlWriter.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");

      xmlWriter.transform(new DOMSource(XmlDoc), new StreamResult(arquivo));
    }catch(Exception e){
        throw new Exception("salvaDocumentEmArquivo() - Erro ao gravar XML - "+e.getMessage());
    }
  }

  /**
   * 
   * @param XmlDoc
   * @param saida
   * @throws Exception
   */
  public static void salvaDocumentEmOutputStream(Document XmlDoc,
                                                 OutputStream saida) throws Exception{
    try{
      //salva o Document no Arquivos
      TransformerFactory transFabrica = TransformerFactory.newInstance();
      Transformer xmlWriter = transFabrica.newTransformer();
      xmlWriter.setOutputProperty("encoding","UTF-8");
      //as 2 proximas linhas sao para colocar \n no arquivo e ident�-lo
      xmlWriter.setOutputProperty(OutputKeys.INDENT, "yes");
      xmlWriter.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");

      xmlWriter.transform(new DOMSource(XmlDoc), new StreamResult(saida));
    }catch(Exception e){
        throw new Exception("salvaDocumentEmOutputStream() - Erro ao gravar XML - "+e.getMessage());
    }
  }

  /**
   * 
   * @param XmlDoc
   * @return
   * @throws Exception
   */
  public static String transformDocumentEmString(Document XmlDoc) throws Exception{
    ByteArrayOutputStream strSaida = new ByteArrayOutputStream();
    salvaDocumentEmOutputStream(XmlDoc, strSaida);
    return strSaida.toString();
  }



  public static Document getXMLDocFromString(String xmlString) throws Exception{
    ByteArrayInputStream temp = new ByteArrayInputStream(xmlString.getBytes());
    return transformInputStreamToDocument(temp);
  }

	  public static String getValorElementoXmlPelaTag( String tagName, Element elemPai, boolean geraErro ) throws Exception{
		    Element oElem;
		    if (geraErro)
		    	oElem = getElementoXmlEx(tagName, elemPai);
		    else
		    	oElem = getElementoXml(tagName, elemPai);
		    if (oElem != null) {
		    	return getValorElementoXml(oElem, geraErro);
		    } else
		    	return null;
	}

	public static Element crieElemFilhoTexto( String tagName, Document doc, String valor ) throws Exception{
		    Element resposta = doc.createElement(tagName);
		    if (valor != null && !valor.isEmpty()) 
		    	FuncoesXml.setValorElementoXml(resposta, valor, doc);
		    return resposta;
	}
	public static String getTagElemento(Element elem) {
	      String resposta;
	      if (elem.getLocalName() != null && !elem.getLocalName().isEmpty() )
	    	  resposta = elem.getLocalName();
	      else
	    	  resposta = elem.getTagName();
	      return resposta;
	}
	
	public static String transformeElementEmString(Element elemDespesa) throws Exception{
		try {
			Document docAux = criaXmlDocument();
			docAux.appendChild(
					docAux.importNode(elemDespesa, true));
			return transformDocumentEmString(docAux);
	    }catch(Exception e){
	        throw new Exception("(transformeElementEmString) ", e);
      }
	}
  
  /*****************
   * MAIN
   * metodo apenas para teste do metodo getElementoXml
   * @param args
   */
/*
  public static void main(String args[]) throws Exception {
    //cria um builder factory
    DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
    //cria um builder
    DocumentBuilder builder = dbf.newDocumentBuilder();
    //cria um documento a partir do arquivo
    Document xmldoc = builder.parse(new File("d:/home/dorbs/r0_171.xml"));

    //tira o elemento pai
    Element pai = xmldoc.getDocumentElement(); //le o elemento <transacao>

    //pega a lista de elementos e retira somente os elementos servs_solicits
    NodeList nl = xmldoc.getElementsByTagName("resposta");

    if (nl==null)
      throw new Exception ("Tag pai nao existe");
    //retira o primeiro elemento
    pai = (Element)nl.item(0);


    System.out.println("Tag pai: "+pai.getTagName());

    Element element = null;//elemento a ser procurado

    if (pai.hasChildNodes()) {
      element = mkFuncoesXml.getElementoXml("msgs_erro",pai);
      if ( element == null)
        throw new Exception("Elemento nao existe");
      else
        System.out.println("Tag encontrada: "+element.getTagName());
    }
 }//main

*/
}//classe