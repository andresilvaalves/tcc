package org.andre.tcc.jpa.xml;


import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.andre.tcc.jpa.entity.Aviso;
import org.andre.tcc.jpa.enumeration.EnumAviso;
import org.andre.tcc.jpa.enumeration.EnumDirecaoVento;
import org.andre.tcc.jpa.enumeration.EnumTempo;
import org.andre.tcc.jpa.enumeration.EnumTipoAviso;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class XMLUtil {

	/**
	 * <env:Envelope xmlns:env='http://schemas.xmlsoap.org/soap/envelope/'>
	 * 	<env:Header>
	 * 	</env:Header>
	 * 		<env:Body>
	 * 			<ns2:solicitaDadosLocalizacaoResponse xmlns:ns2="http://webservice.tcc.andre.org/">
	 * 				<return>esta retornando 1</return>
	 * 			</ns2:solicitaDadosLocalizacaoResponse>
	 * 		</env:Body>
	 * </env:Envelope>
	 */
	public static void criaXMLEnvio(){
		
	}
	
	
	public static Aviso retorneAviso(String xml) {
		Aviso aviso = new Aviso();
		if (xml != null) {
			try {
				Document document = FuncoesXml.getXMLDocFromString(xml);
				Element element = document.getDocumentElement();
				Element elementAviso = FuncoesXml.getFirstElement(element);

				Element elementoAviso = FuncoesXml.getElementoXml("aviso", elementAviso, true);
				String valorAviso = FuncoesXml.getValorElementoXml(elementoAviso, false);
				if (valorAviso.equals(EnumAviso.AVISO_ALTO.toString())) {
					aviso.setAviso(EnumAviso.AVISO_ALTO);
				} else if (valorAviso.equals(EnumAviso.AVISO_BAIXO.toString())) {
					aviso.setAviso(EnumAviso.AVISO_BAIXO);
				} else if (valorAviso.equals(EnumAviso.AVISO_MEDIO.toString())) {
					aviso.setAviso(EnumAviso.AVISO_MEDIO);
				} else if (valorAviso.equals(EnumAviso.AVISO_MUITO_ALTO.toString())) {
					aviso.setAviso(EnumAviso.AVISO_MUITO_ALTO);
				}
				
				Element elementoIdAviso = FuncoesXml.getElementoXml("id", element, true);
				String valorIdAviso = FuncoesXml.getValorElementoXml(elementoIdAviso, false);
				aviso.setId(Long.parseLong(valorIdAviso));

				
				Calendar calendar = Calendar.getInstance();
				try {
					Element elementoData = FuncoesXml.getElementoXml("data", element, true);
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
					Date date = sdf.parse(FuncoesXml.getValorElementoXml(elementoData, false));
					calendar.setTime(date);
					aviso.setData(calendar);
				} catch (Exception e) {
					e.printStackTrace();
					aviso.setData(calendar);
				}

				Element elementoDescricao = FuncoesXml.getElementoXml("descricao", element, true);
				aviso.setDescricao(FuncoesXml.getValorElementoXml(elementoDescricao, false));
				
				Element elementoDistanciaLatitude = FuncoesXml.getElementoXml("distanciaLatitude", element, true);
				String valorDistanciaLatitude = FuncoesXml.getValorElementoXml(elementoDistanciaLatitude, false);
				aviso.setDistanciaLatitude( valorDistanciaLatitude != null ? Double.parseDouble(valorDistanciaLatitude) : 0);
				
				Element elementoDistanciaLongitude= FuncoesXml.getElementoXml("distanciaLongitude", element, true);
				String valorDistanciaLongitude = FuncoesXml.getValorElementoXml(elementoDistanciaLongitude, false);
				aviso.setDistanciaLongitude(valorDistanciaLongitude != null ? Double.parseDouble(valorDistanciaLongitude) : 0);
				
				Element elementoLatitude= FuncoesXml.getElementoXml("latitude", element, true);
				String valorLatitude = FuncoesXml.getValorElementoXml(elementoLatitude, false);
				aviso.setLatitude(valorLatitude != null ? Double.parseDouble(valorLatitude) : 0);
				
				Element elementoLongitude= FuncoesXml.getElementoXml("longitude", element, true);
				String valorLongitude = FuncoesXml.getValorElementoXml(elementoLongitude, false);
				aviso.setLongitude(valorLongitude != null ? Double.parseDouble(valorLongitude) : 0);
				
				Element elementoMensagem = FuncoesXml.getElementoXml("mensagem", element, true);
				aviso.setMensagem(FuncoesXml.getValorElementoXml(elementoMensagem, false));

				//========Meteorologia==========//
				
				
				Element elementoFenomeno = FuncoesXml.getElementoXml("fenomeno", element, true);
				aviso.getMeteorologia().setFenomeno(FuncoesXml.getValorElementoXml(elementoFenomeno, false));
				
				
				Element elementoVelocidadeVento = FuncoesXml.getElementoXml("velocidadeVento", element, true);
				String valorVelocidadeVento = FuncoesXml.getValorElementoXml(elementoVelocidadeVento, false);
				aviso.getMeteorologia().setVelocidadeVento(valorVelocidadeVento != null ? Double.valueOf(valorVelocidadeVento) : 0);
				
				Element elementDirecaoVento = FuncoesXml.getElementoXml("direcaoVento", element, true);
				String valorDirecaoVento = FuncoesXml.getValorElementoXml(elementDirecaoVento, false);
				if(valorDirecaoVento != null){
					if (valorDirecaoVento.equals(EnumDirecaoVento.LESTE.toString())) {
						aviso.getMeteorologia().setDirecaoVento(EnumDirecaoVento.LESTE);
					}else if(valorDirecaoVento.equals(EnumDirecaoVento.NORTE.toString())){
						aviso.getMeteorologia().setDirecaoVento(EnumDirecaoVento.NORTE);
					}else if(valorDirecaoVento.equals(EnumDirecaoVento.OESTE.toString())){
						aviso.getMeteorologia().setDirecaoVento(EnumDirecaoVento.OESTE);
					}else if(valorDirecaoVento.equals(EnumDirecaoVento.SUL.toString())){
						aviso.getMeteorologia().setDirecaoVento(EnumDirecaoVento.SUL);
					}
				}
				
				Element elementTipoAviso = FuncoesXml.getElementoXml("tipoAviso", element, true);
				String valorTipoAviso = FuncoesXml.getValorElementoXml(elementTipoAviso, false);
				if(valorTipoAviso != null){
					if (valorTipoAviso.equals(EnumTipoAviso.INCENDIO.toString())) {
						aviso.setTipoAviso(EnumTipoAviso.INCENDIO);
					}else if(valorTipoAviso.equals(EnumTipoAviso.TEMPERATURA_MAXIMA.toString())){
						aviso.setTipoAviso(EnumTipoAviso.TEMPERATURA_MAXIMA);
					}else if(valorTipoAviso.equals(EnumTipoAviso.TEMPERATURA_MINIMA.toString())){
						aviso.setTipoAviso(EnumTipoAviso.TEMPERATURA_MINIMA);
					}
				}
				
				Element temperaturaMaxima = FuncoesXml.getElementoXml("temperaturaMaxima", element, true);
				aviso.getMeteorologia().setTemperaturaMaxima(Double.valueOf(FuncoesXml.getValorElementoXml(temperaturaMaxima, false)));

				Element temperaturaMinima = FuncoesXml.getElementoXml("temperaturaMinima", element, true);
				aviso.getMeteorologia().setTemperaturaMinima(Double.valueOf(FuncoesXml.getValorElementoXml(temperaturaMinima, false)));

				Element ElemTempo = FuncoesXml.getElementoXml("tempo", element, true);
				String valorElementoTempo = FuncoesXml.getValorElementoXml(ElemTempo, false);
				if (valorElementoTempo.equals(EnumTempo.CHUVA.toString())) {
					aviso.getMeteorologia().setTempo(EnumTempo.CHUVA);
				} else if (valorElementoTempo.equals(EnumTempo.GEADA.toString())) {
					aviso.getMeteorologia().setTempo(EnumTempo.GEADA);
				} else if (valorElementoTempo.equals(EnumTempo.NUBLADO.toString())) {
					aviso.getMeteorologia().setTempo(EnumTempo.NUBLADO);
				} else if (valorElementoTempo.equals(EnumTempo.PARCIALMENTE_NUBLADO.toString())) {
					aviso.getMeteorologia().setTempo(EnumTempo.PARCIALMENTE_NUBLADO);
				} else if (valorElementoTempo.equals(EnumTempo.SOL.toString())) {
					aviso.getMeteorologia().setTempo(EnumTempo.SOL);
				} else if (valorElementoTempo.equals(EnumTempo.TROVOADA.toString())) {
					aviso.getMeteorologia().setTempo(EnumTempo.TROVOADA);
				}
			} catch (Exception e) {
				System.out.println("Erro ao converter XML em entidade"+e.getMessage());
				e.printStackTrace();
			}
		}
		return aviso;
	}
	
	public static void main(String[] args) {
		System.out.println(retorneAviso("<?xml version='1.0' encoding='iso-8859-1'?><Aviso><aviso>AVISO_BAIXO</aviso><data>2011-06-23T19:45:33-03:00</data><descricao>Descrição do Aviso: </descricao><distanciaLatitude>0.048252</distanciaLatitude><distanciaLongitude>0.048252</distanciaLongitude><id>133</id><latitude>-29.020993</latitude><longitude>-53.564572</longitude><mensagem>Mensagem gerada pelo AvisoDescrição do Aviso: </mensagem><meteorologia><direcaoVento>NORTE</direcaoVento><fenomeno>Sol</fenomeno><id>133</id><temperaturaMaxima>28.0</temperaturaMaxima><temperaturaMinima>18.0</temperaturaMinima><tempo>SOL</tempo><velocidadeVento>50.0</velocidadeVento></meteorologia><tipoAviso>TEMPERATURA_MINIMA</tipoAviso></Aviso>"));
	}
	
}
