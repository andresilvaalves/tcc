package org.andre.tcc.jpa.entity;

import java.io.Serializable;

public interface IEntidade extends Cloneable, Serializable {

	static final Long UNSAVED_VALUE = new Long(-1);
	
	long getId();

	void setId(long id);
}
