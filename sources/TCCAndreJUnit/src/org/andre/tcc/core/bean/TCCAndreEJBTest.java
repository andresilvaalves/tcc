package org.andre.tcc.core.bean;

import javax.naming.InitialContext;

import org.andre.tcc.jpa.entity.Aviso;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TCCAndreEJBTest {
	
	InitialContext ctx;
	  
	  @Before
	  public void setUp() throws Exception {
//		  Properties props = new Properties();
//
//			props.setProperty("java.naming.factory.initial", "com.sun.enterprise.naming.SerialInitContextFactory");
//
//			props.setProperty("java.naming.factory.url.pkgs", "com.sun.enterprise.naming");
//
//			props.setProperty("java.naming.factory.state", "com.sun.corba.ee.impl.presentation.rmi.JNDIStateFactoryImpl");
//
//			// Adicionar host caso o WebService esteja rodando em outra maquina
//			props.setProperty("org.omg.CORBA.ORBInitialHost","localhost");
//
//			// definir a porta ORB para CORBA. default � 3700.
//			props.setProperty("org.omg.CORBA.ORBInitialPort", "3700");
//			ctx = new InitialContext(props);
	  }

	  @After
	  public void tearDown() throws Exception {
	    ctx.close();
	    ctx = null;
	  }
	

	@Test
	public void testSolicitaDadosLocalizacaoDoubleDouble() throws Exception {
		TCCAndreRemote tccAndreRemote = (TCCAndreRemote) ctx.lookup("ejb/TCCAndreEJB");
		Aviso aviso = tccAndreRemote.solicitaDadosLocalizacao(-29.020993, -53.564572);
		System.out.println(aviso.getDescricao());
	}



}
