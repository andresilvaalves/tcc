import junit.framework.Test;
import junit.framework.TestSuite;

import org.andre.tcc.core.bean.TCCAndreEJBTest;
import org.junit.runners.Suite.SuiteClasses;

public class AllTests {

	public static Test suite() {
		TestSuite suite = new TestSuite(AllTests.class.getName());
		//$JUnit-BEGIN$
		suite.addTest(new TestSuite(TCCAndreEJBTest.class));
		
		//$JUnit-END$
		return suite;
	}

}
