package org.andre.tcc.core.bean;
import java.util.Collection;

import javax.ejb.Remote;

import org.andre.tcc.jpa.entity.Aviso;


@Remote
public interface TCCAndreRemote {

	Aviso solicitaDadosLocalizacao(double latitude, double longitude);
	
	Collection<Aviso> solicitaAvisosDiarios();
	
}
