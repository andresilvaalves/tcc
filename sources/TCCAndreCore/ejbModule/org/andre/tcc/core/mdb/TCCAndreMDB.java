package org.andre.tcc.core.mdb;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.MessageDriven;
import javax.interceptor.Interceptors;
import javax.jms.BytesMessage;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import javax.jms.StreamMessage;
import javax.jms.TextMessage;

import org.andre.tcc.core.interceptor.TCCAndreInterceptor;
import org.andre.tcc.core.service.AvisoService;
import org.andre.tcc.jpa.entity.Aviso;

@MessageDriven(mappedName = "queue/TCCAndre",
		activationConfig = { 
		@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"
		) }
)
@Interceptors(TCCAndreInterceptor.class)	
public class TCCAndreMDB implements MessageListener {

	@EJB
	private AvisoService ejbAviso;
	
	@Override
	public void onMessage(Message message) {
		try {
			if (message instanceof ObjectMessage) {
				
				
				Serializable objectMsg = ((ObjectMessage) message).getObject();
				
				if(objectMsg instanceof Collection){
					/**
					 * Limpa a tabela de avisos e Insere uma lista nova de avisos
					 */
					Collection<Aviso> avisos = (Collection)objectMsg;
					//int avisosDeletados = ejbAviso.limpaAvisos();
					//System.out.println("Quantidade de Avisos deletados: "+avisosDeletados);
					ejbAviso.inserts(avisos);//insere os novos avisos
					
				}else if(objectMsg instanceof Aviso){
					/**
					 * Insere o aviso recebido na mensagem
					 */
					Aviso aviso = (Aviso) ((ObjectMessage) message).getObject();
					System.out.println("mensagem recebida corretamente"+aviso.getDescricao());
					ejbAviso.insert(aviso);
				}
				
			}else if(message instanceof StreamMessage){
				
			}else if(message instanceof BytesMessage){
				
			}else if(message instanceof MapMessage){
				
			}else if(message instanceof TextMessage){
				
			}
		} catch (JMSException e) {
			e.printStackTrace();
		}

	}

}
