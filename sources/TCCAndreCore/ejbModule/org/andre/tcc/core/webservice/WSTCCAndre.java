package org.andre.tcc.core.webservice;

import java.util.Collection;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import org.andre.tcc.core.bean.TCCAndreEJBLocal;
import org.andre.tcc.jpa.entity.Aviso;

@WebService
@Stateless(mappedName="ejb/WSTCCAndre")
public class WSTCCAndre {

	@EJB
	private TCCAndreEJBLocal ejb;

	@WebMethod
	@WebResult(name="Aviso")
	public Aviso solicitaDadosLocalizacao(@WebParam(name="latitude") double latitude, 
			@WebParam(name="longitude")double longitude) {
		System.out.println("Aviso Solicitado por WS: Latitude:"+latitude+" Longitude: "+longitude);
		return ejb.solicitaDadosLocalizacao(latitude, longitude);
	}
	
	
	@WebMethod
	@WebResult(name="Aviso")
	public Aviso solicitaDadosLocalizacaoSemParametros() {
		System.out.println("Aviso Solicitado por WS");
		Aviso aviso = null;
		Collection<Aviso> avisosDiarios = ejb.solicitaAvisosDiarios();
		if(avisosDiarios != null && !avisosDiarios.isEmpty()){
			aviso = avisosDiarios.iterator().next();
		}
		return aviso;
	}


}
