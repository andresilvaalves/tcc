package org.andre.tcc.core.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 * <P><B>Descricao da classe:</B><BR>
 * Classe responsavel por implementar os servicos de CRUD do sistema.
 * </P>
 * 
 * @author andre
 */
@SuppressWarnings("unchecked")
public class BasePersistenceServiceImpl<T> implements BasePersistenceService<T> {

    /**
     * EntityManager.
     */
    @PersistenceContext(unitName="TCCAndrePU")
    protected EntityManager entityManager;

    /**
     * @see BasePersistenceService#listAll(Class< T >).
     * 
     * @param classType Class< T >
     * @return List< T >
     */
	public List<T> listAll(final Class<T> classType) {
        List<T> results = null;
        final Query query = this.createQuery("SELECT object(o) FROM " + classType.getSimpleName() + " AS o");
        results = query.getResultList();

        return results;
    }

    /**
     * @see BasePersistenceService#insert(Object).
     * 
     * @param bean Executa a persistencia do objeto.
     * @return Retorna o objeto inserido.
     */
    public T insert(final T bean) {
        entityManager.persist(bean);
        entityManager.flush();
        return bean;
    }

    /**
     * @see BasePersistenceService#update(Object).
     * 
     * @param bean Executa a atualizacao do objeto.
     * @return Retorna o objeto atualizado.
     */
    public T update(final T bean) {
        entityManager.merge(bean);
        entityManager.flush();
        return bean;
    }

    /**
     * @see BasePersistenceService#remove(int).
     * 
     * @param classType Classe do object a ser removido.
     * @param identifier Remove um objeto atraves do identificador.
     */
    public void remove(final Class<T> classType, final Serializable identifier) {
        final T bean = find(classType, identifier);
        if (bean != null) {
            entityManager.remove(bean);
        }
    }

    /**
     * @see BasePersistenceService#find(int).
     * 
     * @param classType Classe do object a ser recuperado.
     * @param identifier Recupera um objeto atraves do id.
     * @return Retorna uma instancia do objeto encontrado.
     */
    public T find(final Class<T> classType, final Serializable identifier) {
        return entityManager.find(classType, identifier);
    }

    /**
     * @see BasePersistenceService#createNamedQuery(String).
     * 
     * @param queryName nome da query 
     * @return Query objeto query.
     */
    public Query createNamedQuery(final String queryName) {
        return entityManager.createNamedQuery(queryName);
    }

    /**
     * @see BasePersistenceService#createQuery(String)
     * 
     * @param querySQL string contendo o SQL a ser utilizado.
     * @return Query objeto query.
     */
    public Query createQuery(final String querySQL) {
        return entityManager.createQuery(querySQL);
    }
    
    /**
     * @see BasePersistenceService#createNativeQuery(String)
     * 
     * @param querySQL string contendo o SQL a ser utilizado.
     * @return Query objeto query.
     */
    public Query createNativeQuery(final String querySQL) {
        return entityManager.createNativeQuery(querySQL);
    }

	@Override
	public Collection<T> inserts(Collection<T> beans) {
		  Collection<T> result = new ArrayList<T>(beans.size());
          for (T bean : beans) {
        	  T bean2 = insert(bean);
              result.add(bean2);
          }
          return result;
	}
	
}
