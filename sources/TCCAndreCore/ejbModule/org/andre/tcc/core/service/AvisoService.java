package org.andre.tcc.core.service;
import java.util.Collection;

import javax.ejb.Local;

import org.andre.tcc.jpa.entity.Aviso;

@Local
public interface AvisoService extends BasePersistenceService<Aviso> {

	/**
	 * Busca avisos pela localização da latitude e longitude
	 * @param latitude
	 * @param longitude
	 * @return
	 */
	Aviso buscaAvisoLocalizacao(double latitude, double longitude);
	
	/**
	 * Metodo que limpa todos os avisos do banco, 
	 * Utilizado quando se pretente limpar registros antigos;
	 * @return
	 */
	int limpaAvisos();
	
	/**
	 * Busca todos os avisos gerados no dia atual.
	 * @return
	 */
	Collection<Aviso> buscaAvisosDiario();
}
