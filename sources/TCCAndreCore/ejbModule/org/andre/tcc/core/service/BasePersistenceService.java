package org.andre.tcc.core.service;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * <P><B>Descricao da interface:</B><BR>
 * Interface responsavel por definir os servicos de persistencia do projeto.
 * </P>
 * 
 * @author andre
 */
public interface BasePersistenceService<T> {

    /**
     * Lista todos os objetos de um determinado tipo especificado.
     * 
     * @param classType tipo de classe da entidade a ser procurada.
     * @return Lista com as entidades
     */
    List<T> listAll(Class<T> classType);

    /**
     * Salva um Objeto bean.
     * 
     * @param bean Executa a persistencia do objeto.
     * @return Retorna o objeto inserido.
     */
    T insert(T bean);
    
    /**
     * 
     * @param beans
     * @return
     */
    Collection<T> inserts(Collection<T> beans);

    /**
     * Atualiza um Objeto bean.
     * 
     * @param bean Executa a atualizacao do objeto.
     * @return Retorna o objeto atualizado.
     */
    T update(T bean);

    /**
     * Remove um objeto pelo identificador.
     * 
     * @param clazz Classe do object a ser removido.
     * @param identificador Remove um objeto atraves do identificador.
     */
    void remove(Class<T> clazz, Serializable identificador);

    /**
     * Busca um objeto atraves do identificador.
     * 
     * @param clazz Classe do object a ser recuperado.
     * @param identificador Recupera um objeto atravves do id.
     * @return Retorna uma instancia do objeto encontrado.
     */
    T find(Class<T> clazz, Serializable identificador);
    
}
