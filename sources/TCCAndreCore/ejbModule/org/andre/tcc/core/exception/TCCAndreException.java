package org.andre.tcc.core.exception;

public class TCCAndreException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8792026244676270809L;

	
	public TCCAndreException(Exception exception) {
		super(exception);
	}
	
	public TCCAndreException(String msg, Exception exception) {
		super(msg,exception);
	}
	
}
