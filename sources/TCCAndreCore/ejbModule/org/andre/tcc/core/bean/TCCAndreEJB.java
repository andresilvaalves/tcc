package org.andre.tcc.core.bean;

import java.util.Collection;

import javax.ejb.EJB;
import javax.ejb.Stateful;
import javax.interceptor.Interceptors;

import org.andre.tcc.core.interceptor.TCCAndreInterceptor;
import org.andre.tcc.core.service.AvisoService;
import org.andre.tcc.jpa.entity.Aviso;

/**
 * Session Bean implementation class TCCAndreEJB
 */
@Stateful(mappedName = "ejb/TCCAndreEJB")
@Interceptors(TCCAndreInterceptor.class)
public class TCCAndreEJB implements TCCAndreEJBLocal {

	@EJB
	private AvisoService avisoEJB;
    /**
     * Default constructor. 
     */
    public TCCAndreEJB() {
    }

    @Override
	public Aviso solicitaDadosLocalizacao(double latitude, double longitude) {
		Aviso aviso = avisoEJB.buscaAvisoLocalizacao(latitude, longitude);
		return aviso;
	}

	@Override
	public Collection<Aviso> solicitaAvisosDiarios() {
		Collection<Aviso> avisosDiario = avisoEJB.buscaAvisosDiario();
		return avisosDiario;
	}
	

}
