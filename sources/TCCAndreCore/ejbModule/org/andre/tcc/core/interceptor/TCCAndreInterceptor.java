package org.andre.tcc.core.interceptor;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.PostActivate;
import javax.ejb.PrePassivate;
import javax.interceptor.InvocationContext;

public class TCCAndreInterceptor {

	
	@PostConstruct
	public void doPostConstruct(InvocationContext ctx){
		this.log("@PostConstruct", ctx);
		this.invocaBean(ctx);
	}
	
	@PreDestroy
	public void doPreDestroy(InvocationContext ctx){
		this.log("@PreDestroy", ctx);
		this.invocaBean(ctx);
	}
	
	@PrePassivate
	public void doPrePassivate(InvocationContext ctx){
		this.log("@PrePassivate", ctx);
		this.invocaBean(ctx);
	}
	
	@PostActivate
	public void doPostActivate(InvocationContext ctx){
		this.log("@PostActivate", ctx);
		this.invocaBean(ctx);
	}
	
	private void log(String evento, InvocationContext ctx){
		System.out.println("capturando: "+evento+" | Objeto: "+ctx.getTarget()+" | TIMER "+ctx.getTimer());
	}
		
	private void invocaBean(InvocationContext ctx){
		try {
			ctx.proceed();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
