package org.andre.tcc.ciram.model;

import org.andre.tcc.ciram.model.enumeration.TipoCTL;

public abstract class AbstractPosicionamentoCTL {

	private int celulas;
	private TipoCTL tipoCTL;
	
	
	public AbstractPosicionamentoCTL(int celulas, TipoCTL tipoCTL) {
		this.celulas = celulas;
		this.tipoCTL = tipoCTL;
	}
	
	public int getCelulas() {
		return celulas;
	}
	public void setCelulas(int celulas) {
		this.celulas = celulas;
	}
	public TipoCTL getTipo() {
		return tipoCTL;
	}
	public void setTipo(TipoCTL tipoCTL) {
		this.tipoCTL = tipoCTL;
	}
	
}
