package org.andre.tcc.ciram.model;

public class ExtratorCTL {

	/**
	 * Nome do arquivo para ser lido
	 */
	private String dSet;
	/**
	 * codigo que representa ponto n�o definido
	 */
	private double undef;
	/**
	 * Titulo do extrator
	 */
	private String title;
	/**
	 * Longitude
	 * A longitude � a dist�ncia ao meridiano de Greenwich medida ao longo do Equador.
	 * Esta dist�ncia mede-se em graus, podendo variar entre 0� e 180� para Este ou para Oeste.
	 * Por exemplo, Lisboa est� � longitude de 9� 8�W, o Rio de Janeiro � longitude de 34� 53�W e Macau � longitude de 113� 56�E. 
	 */
	private LongitudeCTL xdef;
	
	/**
	 * LatitudeCTL
	 * A latitude � a dist�ncia ao Equador medida ao longo do meridiano de Greenwich. 
	 * Esta dist�ncia mede-se em graus, podendo variar entre 0� e 90� para Norte ou para Sul. 
	 * Por exemplo, Lisboa est� � latitude de 38� 4�N, o Rio de Janeiro � latitude de 22� 55�S e Macau � latitude de 22� 27�N. 
	 */
	private LatitudeCTL ydef;
	
	/**
	 * AltitudeCTL
	 * A Terra � aproximadamente esf�rica, com um ligeiro achatamento nos p�los. 
	 * Para se definir a altitude de um ponto sobre a Terra define-se uma esfera --- geoide --- com um raio de 6378 km. 
	 * A altitude num ponto da Terra � a dist�ncia na vertical � superf�cie deste geoide. Por exemplo, 
	 * a altitude m�dia do Aeroporto de Lisboa � de 114 m, mas a altitude m�dia da Holanda � negativa. 
	 */
	private AltitudeCTL zdef;
	
	/**
	 * TempoCLT
	 */
	private TempoCTL tdef;

	public String getdSet() {
		return dSet;
	}

	public void setdSet(String dSet) {
		this.dSet = dSet;
	}

	public double getUndef() {
		return undef;
	}

	public void setUndef(double undef) {
		this.undef = undef;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public LongitudeCTL getXdef() {
		return xdef;
	}

	public void setXdef(LongitudeCTL xdef) {
		this.xdef = xdef;
	}

	public LatitudeCTL getYdef() {
		return ydef;
	}

	public void setYdef(LatitudeCTL ydef) {
		this.ydef = ydef;
	}

	public AltitudeCTL getZdef() {
		return zdef;
	}

	public void setZdef(AltitudeCTL zdef) {
		this.zdef = zdef;
	}

	public TempoCTL getTdef() {
		return tdef;
	}

	public void setTdef(TempoCTL tdef) {
		this.tdef = tdef;
	}
	
}
