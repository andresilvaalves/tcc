package org.andre.tcc.ciram.bean;

import java.util.Collection;
import java.util.Date;

import javax.ejb.Remote;
import javax.ejb.Timer;

@Remote
public interface MonitoramentoCiramtimerRemote {

	void agendarTarefa(Date inicio, long periodo, String tarefa);
	
	void cancelarTarefa(String tarefa);
	
	Collection<Timer> tarefasAgendadas();
}
