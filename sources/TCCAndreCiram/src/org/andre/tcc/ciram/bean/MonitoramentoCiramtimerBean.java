package org.andre.tcc.ciram.bean;

import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerService;
import javax.interceptor.Interceptors;

import org.andre.tcc.ciram.exception.CiramException;
import org.andre.tcc.ciram.interceptor.CiramInterceptor;
import org.andre.tcc.ciram.leitor.LeitorDadosCIRAM;

/**
 * Session Bean implementation class MonitoramentoCiramtimerBean
 */
@Stateless(mappedName="ejb/TimerCiram")
@Interceptors(CiramInterceptor.class)
public class MonitoramentoCiramtimerBean implements MonitoramentoCiramtimerRemote  {

	@Resource
	private TimerService service;
	
	public void agendarTarefa(Date inicio, long periodo, String tarefa){
		service.createTimer(inicio, periodo,tarefa);
	}
	
	public void cancelarTarefa(String tarefa){
		Collection<Timer> timers = service.getTimers();
		Iterator<Timer> iter = timers.iterator();
		while(iter.hasNext()){
			Timer timer = (Timer) iter.next();
			if(timer.getInfo().equals(tarefa)){
				timer.cancel();
				return;
			}
		}
	}
	
    
    @Timeout
    public void executar(Timer timer){
    	LeitorDadosCIRAM  leitorDadosCIRAM = new LeitorDadosCIRAM();
    	try {
			String tarefa = (String) timer.getInfo();
			System.out.println("Executando Tarefa: "+tarefa);
			
			//Monitorando dados do Ciram para enviar Avisos para o JMS
    		leitorDadosCIRAM.monitoreDados();
			
		} catch (CiramException e) {
			e.printStackTrace();
		}
    }

	@Override
	public Collection<Timer> tarefasAgendadas() {
		return service.getTimers();
	}

}
