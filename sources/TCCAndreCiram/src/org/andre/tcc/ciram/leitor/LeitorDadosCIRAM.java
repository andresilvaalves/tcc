package org.andre.tcc.ciram.leitor;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Random;
import java.util.StringTokenizer;

import org.andre.tcc.ciram.exception.CiramException;
import org.andre.tcc.ciram.jms.EnviaAvisoJMS;
import org.andre.tcc.ciram.model.AltitudeCTL;
import org.andre.tcc.ciram.model.ExtratorCTL;
import org.andre.tcc.ciram.model.LatitudeCTL;
import org.andre.tcc.ciram.model.LongitudeCTL;
import org.andre.tcc.ciram.model.TempoCTL;
import org.andre.tcc.ciram.model.enumeration.TipoCTL;
import org.andre.tcc.ciram.teste.AvisosAleatorios;
import org.andre.tcc.jpa.entity.Aviso;
import org.andre.tcc.jpa.entity.Meteorologia;
import org.andre.tcc.jpa.enumeration.EnumAviso;
import org.andre.tcc.jpa.enumeration.EnumDirecaoVento;
import org.andre.tcc.jpa.enumeration.EnumTempo;
import org.andre.tcc.jpa.enumeration.EnumTipoAviso;

public class LeitorDadosCIRAM {
	
	private static final String LINHA_DSET = "DSET",
								LINHA_UNDEF = "UNDEF",
								LINHA_TITLE = "TITLE",
								LINHA_XDEF = "xdef",
								LINHA_YDEF = "ydef",
								LINHA_ZDEF = "zdef",
								LINHA_TDEF = "tdef";	
	
	/**
	 * M�todo resposavel por ler os extratores e guardar em objetos
	 * Os extratores s�o arquivos que possuem as informa��es de como dever� ser lido os arquivos GRD e BIN que possuem informa��es da CIRAM
	 * @param pathname
	 * @return
	 * @throws CiramException
	 */
	private ExtratorCTL leiaExtrator(String pathname) throws CiramException{
		ExtratorCTL extratorCTL = new ExtratorCTL();
		try {
			File file = new File(pathname);
			FileReader fileReader = new FileReader(file);
			BufferedReader in = new BufferedReader(fileReader);
			while(in.ready()){
				String linha = in.readLine();
				StringTokenizer tokenizer = new StringTokenizer(linha);
				String firstToken = tokenizer.nextToken();
				
				if(firstToken.equals(LINHA_DSET)){
					extratorCTL.setdSet(tokenizer.nextToken().replace("^", ""));
				}else if(firstToken.equals(LINHA_UNDEF)){
					String nextToken = tokenizer.nextToken();
					extratorCTL.setUndef(Double.valueOf(nextToken));
				}else if(firstToken.equals(LINHA_TITLE)){
					extratorCTL.setTitle(tokenizer.nextToken());
				}else if(firstToken.equals(LINHA_XDEF)){
					int celulas = Integer.parseInt(tokenizer.nextToken());
					TipoCTL tipo = TipoCTL.valueOf(tokenizer.nextToken().toUpperCase());
					double posicao = Double.valueOf(tokenizer.nextToken());
					double distancia = Double.valueOf(tokenizer.nextToken());
					LongitudeCTL longitudeCTL = new LongitudeCTL(celulas, tipo, posicao,distancia);
					extratorCTL.setXdef(longitudeCTL);
				}else if(firstToken.equals(LINHA_YDEF)){
					//TODO corrigir codigo repetido
					int celulas = Integer.parseInt(tokenizer.nextToken());
					TipoCTL tipo = TipoCTL.valueOf(tokenizer.nextToken().toUpperCase());
					double posicao = Double.valueOf(tokenizer.nextToken());
					double distancia = Double.valueOf(tokenizer.nextToken());
					LatitudeCTL latitudeCTL = new LatitudeCTL(celulas, tipo, posicao,distancia);
					extratorCTL.setYdef(latitudeCTL);
				}else if(firstToken.equals(LINHA_ZDEF)){
					//TODO corrigir codigo repetido
					int celulas = Integer.parseInt(tokenizer.nextToken());
					TipoCTL tipo = TipoCTL.valueOf(tokenizer.nextToken().toUpperCase());
					double posicao = Double.valueOf(tokenizer.nextToken());
					double distancia = Double.valueOf(tokenizer.nextToken());
					AltitudeCTL altitudeCTL = new AltitudeCTL(celulas, tipo, posicao,distancia);
					extratorCTL.setZdef(altitudeCTL);
				}else if(firstToken.equals(LINHA_TDEF)){
					int celulas = Integer.parseInt(tokenizer.nextToken());
					TipoCTL tipo = TipoCTL.valueOf(tokenizer.nextToken().toUpperCase());
					String dtStr = tokenizer.nextToken();
					//TODO ler a data formatada
					TempoCTL tempoCTL = new TempoCTL(celulas,tipo,new Date());
					extratorCTL.setTdef(tempoCTL);
				}
			}
			
		} catch (FileNotFoundException e) {			
			throw new CiramException("Arquivo n�o encontrado. Nome Arquivo: "+pathname, e);
		} catch (IOException e) {
			throw new CiramException("Ocorreu erro ao ler o arquivo. Nome Arquivo: "+pathname, e);
		}
		return extratorCTL;
	}
	
	
	/**
	 * metodo responsavel por ler os dados do arquivo 
	 * @param extratorCTL
	 */
	private void leiaTemperaturas(ExtratorCTL extratorCTL){
		try {
			File file = new File("epagri/"+extratorCTL.getdSet());
//			BufferedReader reader = new BufferedReader(new FileReader(file));
			FileInputStream inputStream = new FileInputStream(file);
			BufferedInputStream in = new BufferedInputStream(inputStream);  
	        ByteArrayOutputStream baos = new ByteArrayOutputStream();
	        
	        int x = 0;  
	        while((x = in.read()) != -1){  
	          baos.write(x);  
	        }  
	        in.close();  
	        
	        String fileContent = new String(baos.toByteArray());
	        System.out.println("Dados="+fileContent+"|");
	        
//			 byte[] b = new byte[(int)file.length()];  
//		      int i=-1;  
//		      while ( (i=inputStream.read()) !=-1){
//		         inputStream.read(b);  
//		         System.out.println(b);
//		      }
	        
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	

	/**
	 * 
	 * @throws CiramException
	 */
	public void monitoreDados() throws CiramException{
		File file = new File("epagri");
		Collection<ExtratorCTL> extratores = new ArrayList<ExtratorCTL>();
		/**
		 * L� primeiro a lista de extratores
		 */
		if(file.exists()){
			String[] listaArquivos = file.list();
			for (String nmArquivo : listaArquivos) {
				if(nmArquivo.endsWith(".ctl")){
					ExtratorCTL extratorCTL = leiaExtrator("epagri/"+nmArquivo);
					extratores.add(extratorCTL);
				}
			}
		}
		/**
		 * Para cada extrator, ir� buscar o arquivo para leitura
		 */
		if(extratores != null){
			for (ExtratorCTL extrator : extratores) {
				leiaTemperaturas(extrator);
			}
		}
		
		/**
		 * TODO Gera avisos automaticamente e adiciona no JMS
		 * usado como Teste por causa que a parte de ler as informa��es do Ciram ainda n�o terminou
		 */
		Collection<Aviso> avisosGeradosAutomaticamente = geraAvisosAutomaticamente();
		
		/**
		 * Envia a lista de avisos para o JMS
		 */
		EnviaAvisoJMS.getInstance().enviaAvisos(avisosGeradosAutomaticamente);
	}


	private Collection<Aviso> geraAvisosAutomaticamente() {
		Collection<Aviso> avisosGeradosAutomaticamente = new ArrayList<Aviso>();
		//
		double longitude = -53.902336;
		double latitude = -29.358757;
		double distancia = 0.048252;
		avisosGeradosAutomaticamente = AvisosAleatorios.avisosAleatorios(latitude, longitude, distancia);
		
		return avisosGeradosAutomaticamente;
	}
	
	
	public static void main(String[] args) {
		LeitorDadosCIRAM leitorDadosCIRAM = new LeitorDadosCIRAM();
		Collection<Aviso> geraAvisosAutomaticamente = leitorDadosCIRAM.geraAvisosAutomaticamente();
		System.out.println(geraAvisosAutomaticamente);
	}

}
