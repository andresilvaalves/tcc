package org.tcc.andre.ciram.web;

import java.io.IOException;
import java.util.Calendar;

import javax.interceptor.Interceptors;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.andre.tcc.ciram.bean.MonitoramentoCiramtimerRemote;
import org.andre.tcc.ciram.interceptor.CiramInterceptor;

/**
 * Servlet implementation class InitServlet
 */
@WebServlet(description = "Servlet TCC Andr� para carregar informa��es da EPAGRI/CIRAM", 
		urlPatterns = { "/InitServlet" })
@Interceptors(CiramInterceptor.class)
public class InitServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InitServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * Ser� iniciado uma vez ao carregar a aplica��o
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		
		try {
			System.out.println("Criando agendamento....");
			//Cria o Agendamento de tarefas
			Context context = new InitialContext();
			MonitoramentoCiramtimerRemote timer = (MonitoramentoCiramtimerRemote) context.lookup("ejb/TimerCiram");
			int periodo = 60;
			Calendar dataInicial = Calendar.getInstance();
			dataInicial.add(Calendar.SECOND, periodo);
			timer.agendarTarefa(dataInicial.getTime(), 0/**periodo * 1000 */, "AgendamentoCiram");
		} catch (NamingException e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
