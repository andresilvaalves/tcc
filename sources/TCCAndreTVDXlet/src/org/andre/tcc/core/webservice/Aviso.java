/**
 * Aviso.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.andre.tcc.core.webservice;

public class Aviso  implements java.io.Serializable {
    private org.andre.tcc.core.webservice.EnumAviso aviso;

    private java.util.Calendar data;

    private java.lang.String descricao;

    private double distanciaLatitude;

    private double distanciaLongitude;

    private long id;

    private double latitude;

    private double longitude;

    private java.lang.String mensagem;

    private org.andre.tcc.core.webservice.Meteorologia meteorologia;

    private org.andre.tcc.core.webservice.EnumTipoAviso tipoAviso;

    public Aviso() {
    }

    public Aviso(
           org.andre.tcc.core.webservice.EnumAviso aviso,
           java.util.Calendar data,
           java.lang.String descricao,
           double distanciaLatitude,
           double distanciaLongitude,
           long id,
           double latitude,
           double longitude,
           java.lang.String mensagem,
           org.andre.tcc.core.webservice.Meteorologia meteorologia,
           org.andre.tcc.core.webservice.EnumTipoAviso tipoAviso) {
           this.aviso = aviso;
           this.data = data;
           this.descricao = descricao;
           this.distanciaLatitude = distanciaLatitude;
           this.distanciaLongitude = distanciaLongitude;
           this.id = id;
           this.latitude = latitude;
           this.longitude = longitude;
           this.mensagem = mensagem;
           this.meteorologia = meteorologia;
           this.tipoAviso = tipoAviso;
    }


    /**
     * Gets the aviso value for this Aviso.
     * 
     * @return aviso
     */
    public org.andre.tcc.core.webservice.EnumAviso getAviso() {
        return aviso;
    }


    /**
     * Sets the aviso value for this Aviso.
     * 
     * @param aviso
     */
    public void setAviso(org.andre.tcc.core.webservice.EnumAviso aviso) {
        this.aviso = aviso;
    }


    /**
     * Gets the data value for this Aviso.
     * 
     * @return data
     */
    public java.util.Calendar getData() {
        return data;
    }


    /**
     * Sets the data value for this Aviso.
     * 
     * @param data
     */
    public void setData(java.util.Calendar data) {
        this.data = data;
    }


    /**
     * Gets the descricao value for this Aviso.
     * 
     * @return descricao
     */
    public java.lang.String getDescricao() {
        return descricao;
    }


    /**
     * Sets the descricao value for this Aviso.
     * 
     * @param descricao
     */
    public void setDescricao(java.lang.String descricao) {
        this.descricao = descricao;
    }


    /**
     * Gets the distanciaLatitude value for this Aviso.
     * 
     * @return distanciaLatitude
     */
    public double getDistanciaLatitude() {
        return distanciaLatitude;
    }


    /**
     * Sets the distanciaLatitude value for this Aviso.
     * 
     * @param distanciaLatitude
     */
    public void setDistanciaLatitude(double distanciaLatitude) {
        this.distanciaLatitude = distanciaLatitude;
    }


    /**
     * Gets the distanciaLongitude value for this Aviso.
     * 
     * @return distanciaLongitude
     */
    public double getDistanciaLongitude() {
        return distanciaLongitude;
    }


    /**
     * Sets the distanciaLongitude value for this Aviso.
     * 
     * @param distanciaLongitude
     */
    public void setDistanciaLongitude(double distanciaLongitude) {
        this.distanciaLongitude = distanciaLongitude;
    }


    /**
     * Gets the id value for this Aviso.
     * 
     * @return id
     */
    public long getId() {
        return id;
    }


    /**
     * Sets the id value for this Aviso.
     * 
     * @param id
     */
    public void setId(long id) {
        this.id = id;
    }


    /**
     * Gets the latitude value for this Aviso.
     * 
     * @return latitude
     */
    public double getLatitude() {
        return latitude;
    }


    /**
     * Sets the latitude value for this Aviso.
     * 
     * @param latitude
     */
    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }


    /**
     * Gets the longitude value for this Aviso.
     * 
     * @return longitude
     */
    public double getLongitude() {
        return longitude;
    }


    /**
     * Sets the longitude value for this Aviso.
     * 
     * @param longitude
     */
    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }


    /**
     * Gets the mensagem value for this Aviso.
     * 
     * @return mensagem
     */
    public java.lang.String getMensagem() {
        return mensagem;
    }


    /**
     * Sets the mensagem value for this Aviso.
     * 
     * @param mensagem
     */
    public void setMensagem(java.lang.String mensagem) {
        this.mensagem = mensagem;
    }


    /**
     * Gets the meteorologia value for this Aviso.
     * 
     * @return meteorologia
     */
    public org.andre.tcc.core.webservice.Meteorologia getMeteorologia() {
        return meteorologia;
    }


    /**
     * Sets the meteorologia value for this Aviso.
     * 
     * @param meteorologia
     */
    public void setMeteorologia(org.andre.tcc.core.webservice.Meteorologia meteorologia) {
        this.meteorologia = meteorologia;
    }


    /**
     * Gets the tipoAviso value for this Aviso.
     * 
     * @return tipoAviso
     */
    public org.andre.tcc.core.webservice.EnumTipoAviso getTipoAviso() {
        return tipoAviso;
    }


    /**
     * Sets the tipoAviso value for this Aviso.
     * 
     * @param tipoAviso
     */
    public void setTipoAviso(org.andre.tcc.core.webservice.EnumTipoAviso tipoAviso) {
        this.tipoAviso = tipoAviso;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Aviso)) return false;
        Aviso other = (Aviso) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.aviso==null && other.getAviso()==null) || 
             (this.aviso!=null &&
              this.aviso.equals(other.getAviso()))) &&
            ((this.data==null && other.getData()==null) || 
             (this.data!=null &&
              this.data.equals(other.getData()))) &&
            ((this.descricao==null && other.getDescricao()==null) || 
             (this.descricao!=null &&
              this.descricao.equals(other.getDescricao()))) &&
            this.distanciaLatitude == other.getDistanciaLatitude() &&
            this.distanciaLongitude == other.getDistanciaLongitude() &&
            this.id == other.getId() &&
            this.latitude == other.getLatitude() &&
            this.longitude == other.getLongitude() &&
            ((this.mensagem==null && other.getMensagem()==null) || 
             (this.mensagem!=null &&
              this.mensagem.equals(other.getMensagem()))) &&
            ((this.meteorologia==null && other.getMeteorologia()==null) || 
             (this.meteorologia!=null &&
              this.meteorologia.equals(other.getMeteorologia()))) &&
            ((this.tipoAviso==null && other.getTipoAviso()==null) || 
             (this.tipoAviso!=null &&
              this.tipoAviso.equals(other.getTipoAviso())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAviso() != null) {
            _hashCode += getAviso().hashCode();
        }
        if (getData() != null) {
            _hashCode += getData().hashCode();
        }
        if (getDescricao() != null) {
            _hashCode += getDescricao().hashCode();
        }
        _hashCode += new Double(getDistanciaLatitude()).hashCode();
        _hashCode += new Double(getDistanciaLongitude()).hashCode();
        _hashCode += new Long(getId()).hashCode();
        _hashCode += new Double(getLatitude()).hashCode();
        _hashCode += new Double(getLongitude()).hashCode();
        if (getMensagem() != null) {
            _hashCode += getMensagem().hashCode();
        }
        if (getMeteorologia() != null) {
            _hashCode += getMeteorologia().hashCode();
        }
        if (getTipoAviso() != null) {
            _hashCode += getTipoAviso().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Aviso.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://webservice.core.tcc.andre.org/", "aviso"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("aviso");
        elemField.setXmlName(new javax.xml.namespace.QName("", "aviso"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://webservice.core.tcc.andre.org/", "enumAviso"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("data");
        elemField.setXmlName(new javax.xml.namespace.QName("", "data"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("descricao");
        elemField.setXmlName(new javax.xml.namespace.QName("", "descricao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("distanciaLatitude");
        elemField.setXmlName(new javax.xml.namespace.QName("", "distanciaLatitude"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("distanciaLongitude");
        elemField.setXmlName(new javax.xml.namespace.QName("", "distanciaLongitude"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("latitude");
        elemField.setXmlName(new javax.xml.namespace.QName("", "latitude"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("longitude");
        elemField.setXmlName(new javax.xml.namespace.QName("", "longitude"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mensagem");
        elemField.setXmlName(new javax.xml.namespace.QName("", "mensagem"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("meteorologia");
        elemField.setXmlName(new javax.xml.namespace.QName("", "meteorologia"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://webservice.core.tcc.andre.org/", "meteorologia"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipoAviso");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipoAviso"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://webservice.core.tcc.andre.org/", "enumTipoAviso"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
