/**
 * EnumAviso.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.andre.tcc.core.webservice;

public class EnumAviso implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected EnumAviso(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _SEM_AVISO = "SEM_AVISO";
    public static final java.lang.String _AVISO_BAIXO = "AVISO_BAIXO";
    public static final java.lang.String _AVISO_MEDIO = "AVISO_MEDIO";
    public static final java.lang.String _AVISO_ALTO = "AVISO_ALTO";
    public static final java.lang.String _AVISO_MUITO_ALTO = "AVISO_MUITO_ALTO";
    public static final EnumAviso SEM_AVISO = new EnumAviso(_SEM_AVISO);
    public static final EnumAviso AVISO_BAIXO = new EnumAviso(_AVISO_BAIXO);
    public static final EnumAviso AVISO_MEDIO = new EnumAviso(_AVISO_MEDIO);
    public static final EnumAviso AVISO_ALTO = new EnumAviso(_AVISO_ALTO);
    public static final EnumAviso AVISO_MUITO_ALTO = new EnumAviso(_AVISO_MUITO_ALTO);
    public java.lang.String getValue() { return _value_;}
    public static EnumAviso fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        EnumAviso enumeration = (EnumAviso)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static EnumAviso fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(EnumAviso.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://webservice.core.tcc.andre.org/", "enumAviso"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
