package org.andre.tcc.core.webservice;

public class WSTCCAndreProxy implements org.andre.tcc.core.webservice.WSTCCAndre {
  private String _endpoint = null;
  private org.andre.tcc.core.webservice.WSTCCAndre wSTCCAndre = null;
  
  public WSTCCAndreProxy() {
    _initWSTCCAndreProxy();
  }
  
  public WSTCCAndreProxy(String endpoint) {
    _endpoint = endpoint;
    _initWSTCCAndreProxy();
  }
  
  private void _initWSTCCAndreProxy() {
    try {
      wSTCCAndre = (new org.andre.tcc.core.webservice.WSTCCAndreServiceLocator()).getWSTCCAndrePort();
      if (wSTCCAndre != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)wSTCCAndre)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)wSTCCAndre)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (wSTCCAndre != null)
      ((javax.xml.rpc.Stub)wSTCCAndre)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public org.andre.tcc.core.webservice.WSTCCAndre getWSTCCAndre() {
    if (wSTCCAndre == null)
      _initWSTCCAndreProxy();
    return wSTCCAndre;
  }
  
  public org.andre.tcc.core.webservice.Aviso solicitaDadosLocalizacao(double latitude, double longitude) throws java.rmi.RemoteException{
    if (wSTCCAndre == null)
      _initWSTCCAndreProxy();
    return wSTCCAndre.solicitaDadosLocalizacao(latitude, longitude);
  }
  
  public org.andre.tcc.core.webservice.Aviso solicitaDadosLocalizacaoSemParametros() throws java.rmi.RemoteException{
    if (wSTCCAndre == null)
      _initWSTCCAndreProxy();
    return wSTCCAndre.solicitaDadosLocalizacaoSemParametros();
  }
  
  
}