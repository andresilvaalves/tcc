/**
 * Meteorologia.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.andre.tcc.core.webservice;

public class Meteorologia  implements java.io.Serializable {
    private org.andre.tcc.core.webservice.EnumDirecaoVento direcaoVento;

    private java.lang.String fenomeno;

    private long id;

    private double temperaturaMaxima;

    private double temperaturaMinima;

    private org.andre.tcc.core.webservice.EnumTempo tempo;

    private double velocidadeVento;

    public Meteorologia() {
    }

    public Meteorologia(
           org.andre.tcc.core.webservice.EnumDirecaoVento direcaoVento,
           java.lang.String fenomeno,
           long id,
           double temperaturaMaxima,
           double temperaturaMinima,
           org.andre.tcc.core.webservice.EnumTempo tempo,
           double velocidadeVento) {
           this.direcaoVento = direcaoVento;
           this.fenomeno = fenomeno;
           this.id = id;
           this.temperaturaMaxima = temperaturaMaxima;
           this.temperaturaMinima = temperaturaMinima;
           this.tempo = tempo;
           this.velocidadeVento = velocidadeVento;
    }


    /**
     * Gets the direcaoVento value for this Meteorologia.
     * 
     * @return direcaoVento
     */
    public org.andre.tcc.core.webservice.EnumDirecaoVento getDirecaoVento() {
        return direcaoVento;
    }


    /**
     * Sets the direcaoVento value for this Meteorologia.
     * 
     * @param direcaoVento
     */
    public void setDirecaoVento(org.andre.tcc.core.webservice.EnumDirecaoVento direcaoVento) {
        this.direcaoVento = direcaoVento;
    }


    /**
     * Gets the fenomeno value for this Meteorologia.
     * 
     * @return fenomeno
     */
    public java.lang.String getFenomeno() {
        return fenomeno;
    }


    /**
     * Sets the fenomeno value for this Meteorologia.
     * 
     * @param fenomeno
     */
    public void setFenomeno(java.lang.String fenomeno) {
        this.fenomeno = fenomeno;
    }


    /**
     * Gets the id value for this Meteorologia.
     * 
     * @return id
     */
    public long getId() {
        return id;
    }


    /**
     * Sets the id value for this Meteorologia.
     * 
     * @param id
     */
    public void setId(long id) {
        this.id = id;
    }


    /**
     * Gets the temperaturaMaxima value for this Meteorologia.
     * 
     * @return temperaturaMaxima
     */
    public double getTemperaturaMaxima() {
        return temperaturaMaxima;
    }


    /**
     * Sets the temperaturaMaxima value for this Meteorologia.
     * 
     * @param temperaturaMaxima
     */
    public void setTemperaturaMaxima(double temperaturaMaxima) {
        this.temperaturaMaxima = temperaturaMaxima;
    }


    /**
     * Gets the temperaturaMinima value for this Meteorologia.
     * 
     * @return temperaturaMinima
     */
    public double getTemperaturaMinima() {
        return temperaturaMinima;
    }


    /**
     * Sets the temperaturaMinima value for this Meteorologia.
     * 
     * @param temperaturaMinima
     */
    public void setTemperaturaMinima(double temperaturaMinima) {
        this.temperaturaMinima = temperaturaMinima;
    }


    /**
     * Gets the tempo value for this Meteorologia.
     * 
     * @return tempo
     */
    public org.andre.tcc.core.webservice.EnumTempo getTempo() {
        return tempo;
    }


    /**
     * Sets the tempo value for this Meteorologia.
     * 
     * @param tempo
     */
    public void setTempo(org.andre.tcc.core.webservice.EnumTempo tempo) {
        this.tempo = tempo;
    }


    /**
     * Gets the velocidadeVento value for this Meteorologia.
     * 
     * @return velocidadeVento
     */
    public double getVelocidadeVento() {
        return velocidadeVento;
    }


    /**
     * Sets the velocidadeVento value for this Meteorologia.
     * 
     * @param velocidadeVento
     */
    public void setVelocidadeVento(double velocidadeVento) {
        this.velocidadeVento = velocidadeVento;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Meteorologia)) return false;
        Meteorologia other = (Meteorologia) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.direcaoVento==null && other.getDirecaoVento()==null) || 
             (this.direcaoVento!=null &&
              this.direcaoVento.equals(other.getDirecaoVento()))) &&
            ((this.fenomeno==null && other.getFenomeno()==null) || 
             (this.fenomeno!=null &&
              this.fenomeno.equals(other.getFenomeno()))) &&
            this.id == other.getId() &&
            this.temperaturaMaxima == other.getTemperaturaMaxima() &&
            this.temperaturaMinima == other.getTemperaturaMinima() &&
            ((this.tempo==null && other.getTempo()==null) || 
             (this.tempo!=null &&
              this.tempo.equals(other.getTempo()))) &&
            this.velocidadeVento == other.getVelocidadeVento();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDirecaoVento() != null) {
            _hashCode += getDirecaoVento().hashCode();
        }
        if (getFenomeno() != null) {
            _hashCode += getFenomeno().hashCode();
        }
        _hashCode += new Long(getId()).hashCode();
        _hashCode += new Double(getTemperaturaMaxima()).hashCode();
        _hashCode += new Double(getTemperaturaMinima()).hashCode();
        if (getTempo() != null) {
            _hashCode += getTempo().hashCode();
        }
        _hashCode += new Double(getVelocidadeVento()).hashCode();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Meteorologia.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://webservice.core.tcc.andre.org/", "meteorologia"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("direcaoVento");
        elemField.setXmlName(new javax.xml.namespace.QName("", "direcaoVento"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://webservice.core.tcc.andre.org/", "enumDirecaoVento"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fenomeno");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fenomeno"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("temperaturaMaxima");
        elemField.setXmlName(new javax.xml.namespace.QName("", "temperaturaMaxima"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("temperaturaMinima");
        elemField.setXmlName(new javax.xml.namespace.QName("", "temperaturaMinima"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tempo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tempo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://webservice.core.tcc.andre.org/", "enumTempo"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("velocidadeVento");
        elemField.setXmlName(new javax.xml.namespace.QName("", "velocidadeVento"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
