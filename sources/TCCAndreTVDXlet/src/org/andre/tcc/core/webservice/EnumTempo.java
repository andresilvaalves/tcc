/**
 * EnumTempo.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.andre.tcc.core.webservice;

public class EnumTempo implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected EnumTempo(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _INDEFINIDO = "INDEFINIDO";
    public static final java.lang.String _SOL = "SOL";
    public static final java.lang.String _NUBLADO = "NUBLADO";
    public static final java.lang.String _PARCIALMENTE_NUBLADO = "PARCIALMENTE_NUBLADO";
    public static final java.lang.String _CHUVA = "CHUVA";
    public static final java.lang.String _GEADA = "GEADA";
    public static final java.lang.String _TROVOADA = "TROVOADA";
    public static final EnumTempo INDEFINIDO = new EnumTempo(_INDEFINIDO);
    public static final EnumTempo SOL = new EnumTempo(_SOL);
    public static final EnumTempo NUBLADO = new EnumTempo(_NUBLADO);
    public static final EnumTempo PARCIALMENTE_NUBLADO = new EnumTempo(_PARCIALMENTE_NUBLADO);
    public static final EnumTempo CHUVA = new EnumTempo(_CHUVA);
    public static final EnumTempo GEADA = new EnumTempo(_GEADA);
    public static final EnumTempo TROVOADA = new EnumTempo(_TROVOADA);
    public java.lang.String getValue() { return _value_;}
    public static EnumTempo fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        EnumTempo enumeration = (EnumTempo)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static EnumTempo fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(EnumTempo.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://webservice.core.tcc.andre.org/", "enumTempo"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
